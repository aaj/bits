Title: 25e anniversaire de Debian
Date: 2018-08-16 17:50
Tags: debian, birthday
Slug: debian-25-anniversary
Author: draft
Translator: French Translation Team
Lang: fr
Status: draft

Fêtons aujourd'hui le 25e anniversaire de Debian ! 


<!-- we need a nice logo here ... -->

![Debian 25](|filename|/images/debian25.png)


Le projet Debian a été fondé en 1993 par Ian Murdock pour être un projet
communautaire réellement libre. Depuis cette date, le projet Debian est
devenu l'un des plus importants et des plus influents projets à code source
ouvert. Des milliers de volontaires du monde entier travaillent ensemble
pour créer et maintenir les logiciels Debian. Traduite en soixante-dix
langues et gérant un grand nombre de types d'ordinateurs, la distribution
Debian est vraiment le « système d'exploitation universel ». 


<ul>
<li>Plus de 865 243 118 lignes de code source entre Hamm publiée en 1998 et
Stretch publiée en 2017. Plus de 1 605 945 628 lignes de code source entre
Hamm et Sid.</li>

<li>Il existe plus de 300 distributions dérivées de Debian.</li>

</ul>


<!-- some facts about Debian -->


Si vous êtes proches d'une des villes qui
[célèbrent la journée Debian 2018](https://wiki.debian.org/DebianDay/2018),
vous êtes vraiment les bienvenus pour partager cette fête !

Sinon, il est encore temps pour organiser une petite fête ou une contribution
à Debian. 

<!-- paragraph about saying/sending thanks to Debian contributors, or
showing the logo in your webs etc to increase visibility of Debian -->

<!-- how to contribute or join -->


Si vous aimez la conception graphique ou le graphisme en général, jetez un
œil à la page
[https://wiki.debian.org/Design](https://wiki.debian.org/Design) et rejoignez
l'équipe ! Vous pouvez aussi consulter la liste générale des [équipes Debian](https://wiki.debian.org/Teams)
pour trouver d'autres possibilités de participation au développement de
Debian.

Merci à tous ceux qui ont contribué au développement de notre système
d'exploitation bien-aimé pendant ces vingt-cinq années, et joyeux
anniversaire Debian !
