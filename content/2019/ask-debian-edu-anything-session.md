Title: Ask anything you ever wanted to know about Debian Edu!
Date: 2019-05-29 17:30
Tags: debian edu,debian-meeting
Slug: ask-debian-edu-anything
Author: Jonathan Carter
Status: published
Image: /images/debian-edu-logo.svg
Artist: Christoph Muetze

![Debian Edu](|static|/images/debian-edu-logo.svg)

You have heard about [Debian Edu](https://blends.debian.org/edu/) or [Skolelinux](https://www.skolelinux.de/en/), but do you know exactly what we are doing?

Join us on the [#debian-meeting](https://webchat.oftc.net/?channels=#debian-meeting) channel 
on the [OFTC](https://www.oftc.net/) IRC network
on 03 June 2019 at 12:00 UTC for an introduction to Debian Edu,
a Debian pure blend created to fit the requirements of schools and similar institutions.

You will meet Holger Levsen, contributing to Debian Edu since 2005 and member of development team.
Ask him anything you ever wanted to know about Debian Edu!

Your IRC nick needs to be registered in order to join the channel. Refer to the
[Register your account](https://www.oftc.net/Services/) section on the oftc website
for more information on how to register your nick.

You can always refer to 
the [debian-meeting wiki page](https://wiki.debian.org/IRC/debian-meeting)
for the latest information and up to date schedule.
