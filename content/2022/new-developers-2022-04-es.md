Title: Nuevos desarrolladores y mantenedores de Debian (marzo y abril del 2022)
Slug: new-developers-2022-04
Date: 2022-05-13 17:00
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Translator: 
Status: published


Los siguientes colaboradores del proyecto se convirtieron en Debian Developers en los dos últimos meses:

  * Henry-Nicolas Tourneur (hntourne)
  * Nick Black (dank)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers en los dos últimos meses:

  * Jan Mojžíš
  * Philip Wyett
  * Thomas Ward
  * Fabio Fantoni
  * Mohammed Bilal
  * Guilherme de Paula Xavier Segundo

¡Felicidades a todos!

