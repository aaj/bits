Title: Nuevos desarrolladores y mantenedores de Debian (mayo y junio del 2016)
Slug: new-developers-2016-06
Date: 2016-07-10 17:30
Author: Ana Guerrero Lopez
Tags: project
Lang: es
Status: published

Los siguientes colaboradores del proyecto se convirtieron en Debian Developers
en los dos últimos meses:

  * Josué Ortega (josue)
  * Mathias Behrle (mbehrle)
  * Sascha Steinbiss (satta)
  * Lucas Kanashiro (kanashiro)
  * Vasudev Sathish Kamath (vasudev)
  * Dima Kogan (dkogan)
  * Rafael Laboissière (rafael)
  * David Kalnischkies (donkult)
  * Marcin Kulisz (kula)
  * David Steele (steele
  * Herbert Parentes Fortes Neto (hpfn)
  * Ondřej Nový (onovy)
  * Donald Norwood (donald)
  * Neutron Soutmun (neutrons)
  * Steve Kemp (skx)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers
en los dos últimos meses:

  * Sean Whitton
  * Tiago Ilieve
  * Jean Baptiste Favre
  * Adrian Vondendriesch
  * Alkis Georgopoulos
  * Michael Hudson-Doyle
  * Roger Shimizu
  * SZ Lin
  * Leo Singer
  * Peter Colberg

¡Felicidades a todos!






