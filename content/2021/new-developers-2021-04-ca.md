Title: Nous desenvolupadors i mantenidors de Debian (març i abril del 2021)
Slug: new-developers-2021-04
Date: 2021-05-13 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Translator: 
Status: published


Els següents col·laboradors del projecte han esdevingut Debian Developersen els darrers dos mesos:

  * Jeroen Ploemen (jcfp)
  * Mark Hindley (leepen)
  * Scarlett Moore (sgmoore)
  * Baptiste Beauplat (lyknode)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers en els darrers dos mesos:

  * Gunnar Ingemar Hjalmarsson
  * Stephan Lachnit

Enhorabona a tots!

