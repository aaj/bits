Title: Debian célèbre son 28e anniversaire !
Slug: debianday-2021
Date: 2021-08-16 11:01
Author: Donald Norwood
Artist: Daniel Lenharo de Souza and Valessio Brito
Tags: debian, birthday
Lang: fr
Translator: Jean-Pierre Giraud
Status: published

![Debian28thCard](|static|/images/28thDebian_resize.png)

Aujourd'hui, c'est le vingt-huitième anniversaire de Debian. Nous envoyons toute
notre gratitude et notre affection aux contributeurs, développeurs et utilisateurs
qui ont aidé à cette vision et à ce projet.

Il y a de nombreuses célébrations du [#DebianDay](https://wiki.debian.org/DebianDay/2021)
dans le monde entier, peut-être y en a-t-il une près de chez vous ? À la fin de ce mois,
la célébration continue avec la [#DebConf21](https://debconf21.debconf.org/) qui
se tiendra en ligne du 24 au 28 août 2021.
