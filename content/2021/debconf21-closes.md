Title: DebConf21 online closes
Date: 2021-09-09 15:00
Tags: debconf21, announce, debconf
Slug: debconf21-closes
Author: Laura Arjona Reina and Donald Norwood
Artist: Judit Foglszinger
Status: published

[![DebConf21 group photo - click to enlarge](|static|/images/debconf21_group_small.jpg)](https://wiki.debian.org/DebConf/21/GroupPhoto)


On Saturday 28 August 2021, the annual Debian Developers
and Contributors Conference came to a close.


DebConf21 has been held online for the second time, due to the coronavirus 
(COVID-19) disease pandemic. 

All of the sessions have been streamed, with a variety of ways of participating:
via IRC messaging, online collaborative text documents,
and video conferencing meeting rooms.

With 740 registered attendees from more than 15 different countries and a
total of over 70 event talks, discussion sessions,
Birds of a Feather (BoF) gatherings and other activities,
[DebConf21](https://debconf21.debconf.org) was a large success.

The setup made for former online events involving Jitsi, OBS,
Voctomix, SReview, nginx, Etherpad, a web-based
frontend for voctomix has been improved and used for DebConf21 successfully.
All components of the video infrastructure are free software, and configured through the 
Video Team's public [ansible](https://salsa.debian.org/debconf-video-team/ansible) repository.

The DebConf21 [schedule](https://debconf21.debconf.org/schedule/) included
a wide variety of events, grouped in several tracks:

  * Introduction to Free Software and Debian,
  * Packaging, policy, and Debian infrastructure,
  * Systems administration, automation and orchestration,
  * Cloud and containers,
  * Security,
  * Community, diversity, local outreach and social context,
  * Internationalization, Localization and Accessibility,
  * Embedded and Kernel,
  * Debian Blends and Debian derived distributions,
  * Debian in Arts and Science
  * and other.

The talks have been streamed using two rooms, and several of these activities
have been held in different languages: Telugu, Portuguese, Malayalam, Kannada,
Hindi, Marathi and English, allowing a more diverse audience to enjoy and participate.

Between talks, the video stream has been showing the usual sponsors on the loop, but also
some additional clips including photos from previous DebConfs, fun facts about Debian
and short shout-out videos sent by attendees to communicate with their Debian friends. 

The Debian publicity team did the usual «live coverage» to encourage participation
with micronews announcing the different events. The DebConf team also provided several 
[mobile options to follow the schedule](https://debconf21.debconf.org/schedule/mobile/).

For those who were not able to participate, most of the talks and sessions are already
available through the
[Debian meetings archive website](https://meetings-archive.debian.net/pub/debian-meetings/2021/DebConf21/),
and the remaining ones will appear in the following days.

The [DebConf21](https://debconf21.debconf.org/) website
will remain active for archival purposes and will continue to offer
links to the presentations and videos of talks and events.

Next year, [DebConf22](https://wiki.debian.org/DebConf/22) is planned to be held
in Prizren, Kosovo, in July 2022.

DebConf is committed to a safe and welcome environment for all participants.
During the conference, several teams (Front Desk, Welcome team and Community team)
have been available to help so participants get their best experience
in the conference, and find solutions to any issue that may arise.
See the [web page about the Code of Conduct in DebConf21 website](https://debconf21.debconf.org/about/coc/)
for more details on this.


Debian thanks the commitment of numerous [sponsors](https://debconf21.debconf.org/sponsors/)
to support DebConf21, particularly our Platinum Sponsors:
[**Lenovo**](https://www.lenovo.com),
[**Infomaniak**](https://www.infomaniak.com),
[**Roche**](https://code4life.roche.com),
[**Amazon Web Services (AWS)**](https://aws.amazon.com/)
and [**Google**](https://google.com/).


### About Debian

The Debian Project was founded in 1993 by Ian Murdock to be a truly
free community project. Since then the project has grown to be one of
the largest and most influential open source projects.  Thousands of
volunteers from all over the world work together to create and
maintain Debian software. Available in 70 languages, and
supporting a huge range of computer types, Debian calls itself the
_universal operating system_.

### About DebConf

DebConf is the Debian Project's developer conference. In addition to a
full schedule of technical, social and policy talks, DebConf provides an
opportunity for developers, contributors and other interested people to
meet in person and work together more closely. It has taken place
annually since 2000 in locations as varied as Scotland, Argentina, and
Bosnia and Herzegovina. More information about DebConf is available from
[https://debconf.org/](https://debconf.org).


### About Lenovo

As a global technology leader manufacturing a wide portfolio of connected products,
including smartphones, tablets, PCs and workstations as well as AR/VR devices,
smart home/office and data center solutions, [**Lenovo**](https://www.lenovo.com)
understands how critical open systems and platforms are to a connected world.


### About Infomaniak

[**Infomaniak**](https://www.infomaniak.com) is Switzerland's largest web-hosting company,
also offering backup and storage services, solutions for event organizers,
live-streaming and video on demand services.
It wholly owns its datacenters and all elements critical 
to the functioning of the services and products provided by the company 
(both software and hardware). 


### About Roche

[**Roche**](https://code4life.roche.com/) is a major international
pharmaceutical provider and research company dedicated to personalized
healthcare. More than 100.000 employees worldwide work towards solving some
of the greatest challenges for humanity using science and technology. Roche
is strongly involved in publicly funded collaborative research projects with
other industrial and academic partners and have supported DebConf since 2017.


### About Amazon Web Services (AWS)

[**Amazon Web Services (AWS)**](https://aws.amazon.com) is one of the world's
most comprehensive and broadly adopted cloud platform,
offering over 175 fully featured services from data centers globally
(in 77 Availability Zones within 24 geographic regions).
AWS customers include the fastest-growing startups, largest enterprises
and leading government agencies.


### About Google

[**Google**](https://google.com/) is one of the largest technology companies in the
world, providing a wide range of Internet-related services and products such
as online advertising technologies, search, cloud computing, software, and hardware.

Google has been supporting Debian by sponsoring DebConf for more than
ten years, and is also a Debian partner sponsoring parts 
of [Salsa](https://salsa.debian.org)'s continuous integration infrastructure
within Google Cloud Platform.


### Contact Information

For further information, please visit the DebConf21 web page at
[https://debconf21.debconf.org/](https://debconf21.debconf.org/)
or send mail to <press@debian.org>.
