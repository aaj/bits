Title: DebConf20 in a cruise
Date: 2018-04-01 20:15
Tags: debian, announce, debconf
Slug: debconf20-in-a-cruise
Author: Debian Publicity Team
Status: published

The last editions of [DebConf](https://debconf.org), the annual Debian conference,
have been in unalike places like Heidelberg (Germany), Cape Town (South Africa) and Montreal (Canada).
Next summer [DebConf18](https://debconf18.debconf.org) will happen in Hsinchu (Taiwan)
and the location for [DebConf19](https://wiki.debconf.org/wiki/DebConf19) is already decided: Curitiba (Brazil).
During all these years an idea has been floating in the air (aka the Debian IRC channels)
about organising a DebConf in a cruise.
Today, the Debian Project is happy to announce that a group of Debian contributors
have teamed-up to propose an actual bid for *DebConf20 in a cruise*.

The Cruise Team is confident about their ability to provide a detailed and strong bid by the end of the year.
However, a brief plan and preparation is already done:
the conference would happen in July and August 2020, during a trip around the world
in a **"rolling conference"** scheme. This means that Debian contributors could choose when to arrive
and leave by embarking/disembarking in one of the harbours the boat will stop.
A DebCamp focused in sprinting the development of [Debian blends](https://www.debian.org/blends/)
and an "Open Day" with install parties under the sea and other interesting activities for the wide public is also planned.

There will be a sprint to discuss the bid details during DebConf18 in Hsinchu.
The team has also initiated conversations with several cruise ship companies
and satellite network providers in order to explore the possible venues and connectivity options for the conference.
Interested parties can contact press@debian.org to join the Cruise Team in the preparation of the future conference.
