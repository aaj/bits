Title: Debian turns 27!
Date: 2020-08-16 17:15
Tags: debian, birthday
Slug: debian-turns-27
Author: Laura Arjona Reina
Status: published

Today is Debian's 27th anniversary. We recently [wrote](https://bits.debian.org/2020/07/lets-celebrate-debianday-2020-around-the-world.html)
about some ideas to celebrate the DebianDay,
you can join the party or organise something yourselves :-)

Today is also an opportunity for you to start or resume your
contributions to Debian. For example, you can scratch your creative itch
and suggest a wallpaper to be part of the [artwork for the next release](https://wiki.debian.org/DebianDesktop/Artwork/Bullseye),
have a look at the [DebConf20 schedule](https://debconf20.debconf.org/schedule/)
and register to participate online (August 23rd to 29th, 2020),
or put a [Debian live](https://www.debian.org/CD/live/) image in a DVD or USB
and give it to some person near you, who still didn't discover Debian.

Our favorite operating system is the result of all the work we do together.
Thanks to everybody who has contributed in these 27 years, and happy birthday Debian!
